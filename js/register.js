/**
 * Created by jiayi.hu on 12/1/16.
 */
var code;
var serialization;
var serializeArray = new Object();
var getMemberInfoArray = new Array();
var GetMemberInfo;
var dataModel = {
    url: {
        memberInfo: "http://wx.gemii.cc/personal/getUserInfo?code=",
        _memberInfo: "http://wx.gemii.cc/personal/register"
    },
    getMemberInfo: function () {
        var url = this.url.memberInfo + code;
        var data = this.getData(url);
        return data;
    },
    upMemberInfo: function (Data) {
        var url = this.url._memberInfo;
        var data = this.postData(url, Data);
        return data;
    },
    postData: function (url, Data) {
        var arraydata;
        $.ajax({
            data: Data,
            type: "POST",
            url: url,
            dataType: "json",
            async: false,
            cache: false,
            success: function (json) {
                arraydata = eval(json)
            },
            error: function () {
            }
        });
        return arraydata;
    },
    getData: function (url) {
        var arraydata;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "json",
            async: false,
            cache: false,
            success: function (json) {
                arraydata = eval(json)
            },
            error: function () {
            }
        });
        return arraydata;
    }
}
var controller = {
    //null转为字符串
    null2string: function (value) {
        var data = value == null ? "" : value;
        return data;
    },
    dataEcho: function () {
        //数据存下用于回显
        getMemberInfoArray.splice(0, getMemberInfoArray.length);
        $(".userInfo_list").find("tr").each(function () {
            getMemberInfoArray.push($(this).find(".td_second").text());
        })
        console.log(getMemberInfoArray);
        $(".fill_content_text").each(function (i) {
            if (i == 0) {
                $(this).find("input[type='text']").get(0).value = getMemberInfoArray[0];
            }
            if (i == 1) {
                $(this).find("input[type='text']").get(0).value = getMemberInfoArray[1];

            }
            if (i == 2) {
                $(this).find("input[type='text']").get(0).value = getMemberInfoArray[2];
            }
        })
    },
    dataDisplay: function (member_info) {
        $(".userInfo_list").find("tr").each(function (i) {
            if (i == 0) {
                $(this).find(".td_second").empty();
                $(this).find(".td_second").append(controller.null2string(member_info.username));

            }
            if (i == 1) {
                $(this).find(".td_second").empty();
                $(this).find(".td_second").append(member_info.telephone);
            }
            if (i == 2) {
                $(this).find(".td_second").empty();
                $(this).find(".td_second").append(member_info.address);
            }
        })
    },
}
function load() {
    //初始化页面
    var clientHeight = document.documentElement.clientHeight;
    console.log(clientHeight);
    $(".container").css({"height": clientHeight + "px"});
    $(".fill_userInfo").css({"margin-top": (clientHeight - 630) / 2 + "px"});
    //URL解析
    function GetQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)
            return (r[2]);
        return null;
    }

    code = GetQueryString('code');
    GetMemberInfo = dataModel.getMemberInfo();
    console.log(JSON.stringify(GetMemberInfo));
    if (!(GetMemberInfo == null || GetMemberInfo == undefined)) {
        // $(".wechatAvatar").empty();
        $(".wechatName").empty();
        $(".userInfo_list>a").empty();
        $(".profileIntegral>.integral_number").empty();
        $(".userAvatar").attr("src", GetMemberInfo.data.headimgurl);
        $(".wechatName").append(unicode2string(GetMemberInfo.data.nickname));
        if (GetMemberInfo.data.member == undefined) {
            controller.dataDisplay({"username": "", "telephone": "", "address": ""});
            setTimeout(function () {
                $(".userInfoForm_modal").fadeIn(200);
                controller.dataEcho();
            }, 2000);
        } else {
            controller.dataDisplay(GetMemberInfo.data.member);
            setTimeout(function () {
                if (GetMemberInfo.data.member.username == null || GetMemberInfo.data.member.username == ""
                    || GetMemberInfo.data.member.telephone == null || GetMemberInfo.data.member.telephone == ""
                    || GetMemberInfo.data.member.address == null || GetMemberInfo.data.member.address == "") {
                    $(".userInfoForm_modal").fadeIn(200);
                    controller.dataEcho();
                }
            }, 2000);
        }
    } else {
        setTimeout(function () {
            if (GetMemberInfo == undefined) {
                $(".userInfoForm_modal").fadeIn(300);
                controller.dataEcho();
            }
        }, 2000);
    }
}
$(document).ready(function () {
    $(".show_edit").get(0).addEventListener("touchstart", function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(".userInfoForm_modal").fadeIn(100);
        controller.dataEcho();
    })
    $(".fill_close").get(0).addEventListener("touchstart", function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(".userInfoForm_modal").fadeOut(200);
    })
    $(".fill_submit").get(0).addEventListener("touchend", function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log($(".userAvatar"));
        serialization = $("#profileForm").serialize() + "&headimgurl=" + $(".userAvatar").get(0).src;
        var mather_name = $("#mather_name").get(0).value;
        var mobile = $("#mobile").get(0).value;
        var address = $("#address").get(0).value;
        // var babyDate = $("#fill_content_text>span").get(0).innerText;
        serializeArray = {"username": mather_name, "telephone": mobile, "address": address};
        console.log(serializeArray);
        if ($("#mather_name").val() == "" || $("#mobile").val() == "" || $("#address").val() == "") {
            alert("请完成信息填写");
        }
        else {
            if (!(/^1[34578]\d{9}$/.test($("#mobile").val()))) {
                alert("手机号格式不正确");
            }
            else {
                $(".userInfoForm_modal").fadeOut(0);
                var memberInfo = dataModel.upMemberInfo(serialization);
                if (memberInfo.status == 200) {
                    controller.dataDisplay(serializeArray);
                    $(".success_img_modal").fadeIn(200);
                } else {
                    alert("提交失败!");
                }
            }
        }
    })


})