/**
 * Created by jiayi.hu on 12/2/16.
 */
var GB2312UnicodeConverter={
    ToUnicode:function(str){
        var txt= escape(str).toLocaleLowerCase().replace(/%u/gi,'\\u');
        //var txt= escape(str).replace(/([%3F]+)/gi,'\\u');
        return txt.replace(/%7b/gi,'{').replace(/%7d/gi,'}').replace(/%3a/gi,':').replace(/%2c/gi,',').replace(/%27/gi,'\'').replace(/%22/gi,'"').replace(/%5b/gi,'[').replace(/%5d/gi,']').replace(/%3D/gi,'=').replace(/%20/gi,' ').replace(/%3E/gi,'>').replace(/%3C/gi,'<').replace(/%3F/gi,'?').replace(/%5c/gi,'\\');//
    }
    ,ToGB2312:function(str){
        return unescape(str.replace(/\\u/gi,'%u'));
    }
}
function unicode2string(value){
    var text = value.trim();
    // text = text.replace(/\u/g,"");
    return GB2312UnicodeConverter.ToGB2312(text);
}

function string2unicode(value){
    var text =  value.trim();
    // text = text.replace(/\u/g,"");
    return GB2312UnicodeConverter.ToUnicode(text);
}